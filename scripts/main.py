from neo4j import GraphDatabase
from itertools import product

def rank_graph(tx, writeProperty='pageRank', maxIterations=100,
      dampingFactor=0.85):
    _ = tx.run("""
    CALL gds.graph.project('personsNative', 'Person', 'FRIEND')
    """)
    result = tx.run("""
    CALL gds.pageRank.write('personsNative', {
      writeProperty: $writeProperty,
      maxIterations: $maxIterations,
      dampingFactor: $dampingFactor
    })
    YIELD nodePropertiesWritten, ranIterations
    """, writeProperty=writeProperty, maxIterations=maxIterations, dampingFactor=dampingFactor)
    return result
def create_graph(tx, counter1, counter2, name1, name2):
    result = tx.run("""
    MERGE (p$counter1:Person {name: $name1})
    MERGE (p$counter2:Person {name: $name2})
    MERGE (p$counter1)-[:FRIEND]-(p$counter2)
    """, counter1=counter1, counter2=counter2, name1=name1, name2=name2)
    return result
class Scenarios:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        self.driver.close()

    def page_rank(self):
        with self.driver.session() as session:
            graph = session.execute_write(rank_graph)
        print('EXECUTED: page_rank')

    def add_graph(self, lst, name, sur):
        iter1 = iter(product(name, sur))
        id_name_map = {}
        with self.driver.session() as session:
            for count, tuple in enumerate(lst):
                if tuple[0] not in id_name_map: id_name_map[tuple[0]]= ' '.join(next(iter1))
                if tuple[1] not in id_name_map: id_name_map[tuple[1]]= ' '.join(next(iter1))
                graph = session.execute_write(create_graph, tuple[0], tuple[1],
                                              id_name_map[tuple[0]], id_name_map[tuple[1]])
        print('EXECUTED: add_graph')




        # result.single()[0]


if __name__ == "__main__":
    scenarios = Scenarios("bolt://localhost:7687", "neo4j", "Password_0")
    read_list = []
    with open('facebook_combined.txt') as f:
        for line in f:
            line = line[:-1]
            read_list.append(tuple(line.split(' ')))
    print(read_list)
    name_list = []
    with open('name.txt') as f:
        for line in f:
            line = line[:-1]
            name_list.append(line)
    print(name_list)
    sur_list = []
    with open('sur.txt') as f:
        for line in f:
            line = line[:-1]
            sur_list.append(line)
    print(sur_list)
    # scenarios.add_graph(read_list, name_list, sur_list)
    scenarios.page_rank()
    scenarios.close()
